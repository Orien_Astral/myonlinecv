
<?php

$nabarItems = array("", "", "", "");

switch ($page) {
  case 'accueil':
    $navbarItems[0] = "active";
    break;

  case 'competences':
    $navbarItems[1] = "active";
    break;

  case 'diplome':
    $navbarItems[2] = "active";
    break;

  case 'projets':
    $navbarItems[3] = "active";
    break;

  default:
    $navbarItems[0] = "active";
    break;
}

 ?>

<nav>
  <div class="navbar-item <?= $navbarItems[0] ?>">
    <a href="./index.php">
      <span class="<?= $navbarItems[0] ?>">Qui suis-je ?</span>
    </a>
  </div>
  <div class="navbar-item <?= $navbarItems[1] ?>">
    <a href="./index.php?page=competences">
      <span class="<?= $navbarItems[1] ?>">Mes Compétences</span>
    </a>
  </div>
  <div class="navbar-item <?= $navbarItems[2] ?>">
    <a href="./index.php?page=diplome">
      <span class="<?= $navbarItems[2] ?>">Mes Diplômes et Formations</span>
    </a>
  </div>
  <div class="navbar-item <?= $navbarItems[3] ?>">
    <a href="./index.php?page=projets">
      <span class="<?= $navbarItems[3] ?>">Mes projets</span>
    </a>
  </div>
</nav>
