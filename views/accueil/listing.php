<div id="boite-presentation" class="opacity-transition">
  <div id="boite-img-profile" class="">
    <img id="img-profile" class="shadow-transition" src="./public/images/profil picture modifier.jpg" alt="une photo de rhayan tertereau">
  </div>
  <div id="boite-presentation-text" class="shadow-transition">
    <h2 class="title">Présentation :</h2>
    <p>Bonjour à tous, je me nomme Rhayan Tertereau et je suis un jeune développeur qui cherche à s'implanter dans la vie active.
    Je suis passionné de jeux de logique depuis mon enfance ( énigmes ), j'ai découvert le code grace à un ami et depuis j'ai décidé d'en faire mon métier.
    j'ai donc suivi des études dans le domaine du développement et j'ai choisi de me spécialiser dans le développement web.</p>
    <p>Je suis travailleur et volontaire, je suis autonome même si j'aprécie beaucoup le travaille de groupe.</p>
  </div>
</div>
<div id="boite-mes-passions" class="shadow-transition opacity-transition">
  <h2 class="title">Mes Passions</h2>
  <p>Le développement n'est pas ma seul passion. Je suis également passionné de musique et de diverse activités manuelles tel que le tricot et le crochet.
  Même si c'est dans de moindres mesures j'apprécie également la lecture que l'histoire est captivante et touchante.</p>
</div>
<div id="boite-video-presentation" class="shadow-transition opacity-transition">
  <h2 class="title">Vidéos</h2>
  <p>Voici quelques exemples de contenu que j'apprécie</p>
  <div id="boite-video" class="">
    <iframe width="640" height="360" src="https://www.youtube.com/embed/6jSLH9CDPPQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <iframe width="640" height="360" src="https://www.youtube.com/embed/yH-6SoFYkLg?list=RDEcYvmfibc8E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <iframe width="640" height="360" src="https://www.youtube.com/embed/_v4ke8dyvVU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <iframe width="640" height="360" src="https://www.youtube.com/embed/-zkM0wqt_xU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
</div>
