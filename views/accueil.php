<div class="titre">
  <h1>Rhayan Tertereau</h1>
</div>

<?php require_once "./views/header.php" ?>

<div class="container div-body">

  <!-- carte d'identité -->

  <div id="identity-card" class="div-body">
    <div class="">
      <img src="./public/images/furry-head-bleu-blanc.jpg" alt="Ma photo de profil" width="150vw">
    </div>
    <div class="">
      <ul>
        <li>Prénom : Rhayan</li>
        <li>Nom : Tertereau</li>
        <li>Age: 19 Ans</li>
        <li>Téléphone : <a href="tel:+33768480722">07.68.48.07.22</a></li>
        <li>Mail : <a href="mailto:rhayan.tertereau@gmail.com" >rhayan.tertereau@gmail.com</a></li>
      </ul>
    </div>
  </div>
  <div id="description" class="">
    <h4>Description</h4>
    <p>Je suis un développeur passioné plutôt spécialisé dans le web ( PHP / HTML / CSS / JavaScript ) mais qui n'est pas fermé à d'autres langages.
    Je suis un travailleur autonome qui sais aussi bien travailler seul qu'en équipe, motivé et entousiaste.</p>
  </div>
</div>

<div id="experience" class="container div-body">
  <div class="">
    <h4>Expérience Professionelle</h4>
  </div>
</div>
