<?php

/* -- variables -- */

$pages = ["accueil" => AccueilController::class,
          "competences" => CompetencesController::class,
          "diplome" => DiplomesController::class,
          "projets" => ProjetsController::class]; // liste des pages et controllers du site
$page = "accueil"; // page de base
$action = "listing"; // action de base

/* -- récupération de la page et de l'action -- */

if (isset($_GET['page']) && !is_null($_GET['page'])) {
  $page = $_GET['page']; // on alloue la page si elle a été passé en parametre dans l'url
}

if (isset($_GET['action']) && !is_null($_GET['action'])) {
  $action = $_GET['action']; // de même pour l'action
}

/* -- appelle de la page demandé -- */

if(array_key_exists($page, $pages)){ // on regarde dans la liste des pages si celle demandée éxiste
  require_once './controllers/'.$pages[$page].'.php'; // appelle le contronller de la page
  $controller = new $pages[$page](); // instancie le controller
  $vue = $controller->{$action}(); // appelle de la méthode correspondante à l'action
  require_once './base.php'; // on appelle notre fichier qui sert de base a chaque pages
}
else{
  require_once "./views/404.php"; // si la page demandée ne correspond a aucune de la liste alors on appelle la 404
}

?>
