<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Rhayan Tertereau - CV</title>
    <link rel="icon" href="./public/images/22217wolfface_98825.ico">
    <link rel="stylesheet" href="./public/css/index.css">
    <link rel="stylesheet" href="./public/css/<?= $page ?>.css">
  </head>
  <body>

    <section>
      <?php require_once "./views/header.php"; ?>
    </section>

    <section>
      <?php require_once "./views/$page/$action.php"; ?>
    </section>

    <section>
      <?php require_once "./views/header.php"; ?>
    </section>

    <script type="text/javascript" src="./public/js/<?= $page ?>.js"></script>
    <script type="text/javascript" src="./public/js/index.js"></script>
  </body>
</html>
